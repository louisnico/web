function toc() {

  // Create new table element
  var newTable = document.createElement("table");
  newTable.setAttribute("id", "tbl");
  // Insert a new row <tr>, then Add a column in the row
  var row = newTable.insertRow(0);
  var cell = row.insertCell(0);
  
  /* Get all <h1> elements, then
  Iterate over to assign attribute id */
  var h1Eles = document.getElementsByTagName("h1");       
  for(i=0; i < h1Eles.length; i++) {
    
    /* Initiate attribute id with increment number, 
    the assign attribute ID to each h1 */
    var attrString = "h1" + i;
    h1Eles[i].setAttribute("id", attrString);

    // Create anchor tag <a>
    var anchor = document.createElement("a");
    anchor.setAttribute("href", "#" + attrString);

    // Create tag <h1>, then add as Anchor tag's child
    var tagH1 = document.createElement("h1");
    tagH1.innerHTML = h1Eles[i].innerHTML;
    anchor.appendChild(tagH1);

    // Add Event listener to anchor tag
    anchor.addEventListener("mouseover", mousehover);
    anchor.addEventListener("mouseleave", mouseleave);
    
    // Add anchor tag to element td
    cell.append(anchor);

    // Find next sibling of current h1
    h2Eles = findNextSibling(h1Eles[i].getAttribute("id"))

    if (h2Eles.length > 0) {
      for(j=0; j < h2Eles.length; j++) {
        // Create tag <h1>, then add as Anchor tag's child
        var tagH2 = document.createElement("h2");
        var tagA = document.createElement("a");
        var attrIdH2 = "h2" + j;

        // Set id to h2
        h2Eles[j].setAttribute("id", attrIdH2);

        tagA.setAttribute("href", "#" + attrIdH2);
        tagA.innerHTML = h2Eles[j].innerHTML;
        tagH2.appendChild(tagA);

        // Add Event listener to anchor tag
        tagA.addEventListener("mouseover", mousehover);
        tagA.addEventListener("mouseleave", mouseleave);

        cell.append(tagH2);
      }
    }
  }

  // Insert the table element before first element <h1>
  document.body.insertBefore(newTable, h1Eles[0]);
}

function nextSiblingElement(element) {
  
  var siblings = element.nextSiblingElement;
  var h2Siblings = new Array();

  for(i=0; i < siblings.length; i++) {
      h1[i].setAttribute("id", "h1" + i);
  }

  /* var h2Eles = document.getElementsByTagName("h2");
  for(i=0; i < h2Eles.length; i++) {
    h2Eles[i].setAttribute("id", "h2" + i);
  } */
}

function mousehover() {
  var idAttr = this.getAttribute("href").replace("#", "");
  var h1Ele = document.getElementById(idAttr);
  h1Ele.setAttribute("class", "mousehover")
}

function mouseleave() {
  var idAttr = this.getAttribute("href").replace("#", "");
  var h1Ele = document.getElementById(idAttr);
  h1Ele.removeAttribute("class")
}

function findNextSibling(eleID) {

  var _nextSibling = document.getElementById(eleID).nextElementSibling;
  var listSibling = new Array();

  while((_nextSibling != "undefined") && (_nextSibling != null) && (_nextSibling.nodeName != "H1")) {
    if (_nextSibling.nodeName == "H2"){
      listSibling.push(_nextSibling)
    }
    _nextSibling = _nextSibling.nextElementSibling;
  }
  
  return listSibling;
}

window.onload = toc;